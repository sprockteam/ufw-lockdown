#!/bin/bash
export PATH='/bin:/usr/bin:/usr/sbin'

ipregex='^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(\/([0-9]|1[0-9]|2[0-9]|3[0-2]))?$'

declare -A SITES=(
  # Key is a hostname for the site, FQDN or IP address
  # Can use CIDR notation for IP
  # Key is also used as the comment for the UFW rule
  # Value is the UFW app profile to use
  # See Application Integration section here: https://help.ubuntu.com/lts/serverguide/firewall.html.en
  # Examples:
  # ['35.166.50.168/29']='unifi-min'
  # ['34.215.143.99']='unifi-all'
  # ['abc']='unifi-all'
  # ['xyz']='unifi-all'
  # ['admin']='unifi-all-ssh'
  # If not using a common DNS suffix (see below) then use FQDN
  # Example: ['site.mysite.com']='unifi-all'
)

for site in "${!SITES[@]}"
do
  # Use suffix if all sites share a common DNS suffix (optional)
  # Example: suffix='.myddns.com'
  # Site abc would become abc.myddns.com
  # Site xyz would become xyz.myddns.com
  suffix=''

  # If the site key matches the pattern of an IP address, use that
  # Otherwise, try to resolve the IP address
  addr=$(dig +short $site$suffix) && [[ $site =~ $ipregex ]] && addr=$site

  # Determine if the site is currently listed in UFW, if so get the current address
  curraddr=$(ufw status | grep "# $site\$" | awk '{print $3}')

  # If the site is currently listed in UFW, get the rule number
  num=$(ufw status numbered | grep "# $site\$" | awk -F "[][]" '{print $2}')

  # Marks if the address used by UFW should be updated
  update='' && [[ "$addr" != "$curraddr" ]] && update=true

  # If an address for the site was found then proceed
  if [[ $addr ]]
  then
    # If there is a current UFW rule and the address for the site has changed, delete the current rule
    # This is so stale IP addresses don't stay in UFW if the site has a dynamic IP
    if [[ $num && $update ]]
    then
      ufw --force delete $num
    fi
    # If this is a brand new UFW entry, or if the address for the site has changed, add the UFW rule
    if [[ ! $num || $update ]]
    then
      ufw allow from $addr to any app ${SITES[$site]} comment $site
      ufwreload=true
    fi
  fi
done

# Reload UFW if changes were made for good measure
if [[ $ufwreload ]]
then
  ufw reload
fi