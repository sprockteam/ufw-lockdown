# UFW Lockdown

A Bash script used to periodically update UFW to only allow incoming connections from approved sites. Can be used for UniFi or UNMS.

## Example Installation
```console
~$ git clone --depth 1 https://sprocktech@bitbucket.org/sprockteam/ufw-lockdown.git
~$ cd ufw-lockdown
~$ cp allowedsites-sample.sh allowedsites.sh
~$ chmod +x allowedsites.sh
```

Customize allowedsites.sh and the app profiles for your controller and sites. Then install UFW app profiles, reset UFW (optional), test script, re-enable UFW if needed and add to cron:
```console
~$ sudo cp unifi-profiles /etc/ufw/applications.d
~$ sudo ufw reset
~$ sudo ./allowedsites.sh
~$ sudo ufw enable
~$ sudo crontab -e
```

Add your cron line:
```
*/10 * * * * /home/user/ufw-lockdown/allowedsites.sh >/dev/null 2>/home/user/ufw-lockdown/allowedsites.log
```